import sys

N = 15
BONUS = 1000
empty = ''.join(15*[' '])

if len(sys.argv) < 3:
    print "python tester.py test solution"
    exit(1)

test = open(sys.argv[1]).readlines()

board = [ [ test[N-1-x][y] for x in range(N)] for y in range(N)]

sol = map(lambda s: map(int,s.split(" ")),open(sys.argv[2]).readlines())

dx = [0,1,0,-1]
dy = [1,0,-1,0]

def getComp(board,x0,y0):
    stack = [(x0,y0)]
    comp = [(x0,y0)]
    done = set([(x0,y0)])
    while len(stack) != 0:
        x,y = stack[-1]
        stack = stack[:-1]
        for d in range(4):
            x2 = x + dx[d]
            y2 = y + dy[d]
            if x2 >= 0 and x2<N and y2>=0 and y2<N and board[y2][x2] == board[y0][x0] and (x2,y2) not in done:
                stack = stack + [(x2,y2)]
                comp = comp + [(x2,y2)]
                done.add((x2,y2))
    return comp

def printBoard(board):
    for x in range(N):
        for y in range(N):
            print board[y][N-1-x],
        print

printBoard(board)

score = 0

for x0,y0 in sol:
    print "=========================="
    if x0 < 0 or x0 >= N or y0 < 0 or y0 >= N:
        print "Invalid coordinates"%(x0,y0)
        exit(1)
    if board[y0][x0] == ' ':
        print "No piece at %d,%d"%(x0,y0)
        exit(1)
    comp = getComp(board,x0,y0)
    if (len(comp) < 2):
        print "Component of size %d"%(len(comp))
        exit(1)
    score = score + (len(comp)-2)*(len(comp)-2)
    print comp
    for x,y in comp:
        board[y][x] = ' '
    printBoard(board)
    print "--------------------------"
    for y in range(N):
        for x in range(N-1,-1,-1):
            if board[y][x] == ' ':
                board[y] = board[y][:x]+board[y][(x+1):]+[' ']
    for y in range(N-1,-1,-1):
        if ''.join(board[y]) == empty:
            board = board[:y]+board[(y+1):]+[list(empty)]
    printBoard(board)

pieces = "".join(map(lambda l: "".join(l),board))
counts = map(lambda c : pieces.count(c),'01234')
penalty = sum([(x-2)*(x-2) for x in counts if x>0])
if sum(counts) == 0:
    bonus = BONUS
else:
    bonus = 0
total = score - penalty + bonus

print "Total score: %d Base score: %d Penalty: %d Bonus: %d"%(total,score,penalty,bonus) 

