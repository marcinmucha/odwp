#include <string>
#include <set>
#include <thread>
#include <cassert>
#include <iostream>

using namespace std;

unsigned char blue[] = {0,0,255};
unsigned char red[] = {255,0,0};
unsigned char black[] = {0, 0, 0};

const int N = 10000;
int t[N][N];
int color[N];
int n;

void read_vertices(char *s) {
  FILE *f = fopen(s, "r");
  int m;
  fscanf(f, "%d %d", &n, &m);
  fprintf(stderr, "n=%d m=%d\n", n, m);
  assert(n < N);
  for (int i = 0; i < m; ++i) {
    int a, b;
    fscanf(f, "%d %d", &a, &b);
    t[a][b] = t[b][a] = 1;
  }
  fclose(f);
}


void read_solution(char *s) {
  FILE *f = fopen(s, "r");
  set<int> colors;
  for (int i = 0; i < n; ++i) {
    fscanf(f, "%d", color+i);
    colors.insert(color[i]);
  }
  for (int i = 0; i < n; ++i) for (int j = 0; j < i; ++j) assert(t[i][j] == 0 || color[i] != color[j]);
  fprintf(stderr, "OK %d colors used\n", (int)colors.size());
  fclose(f);
}

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "Usage %s infile outfile\n", argv[0]);
    return 1;
  }
  read_vertices(argv[1]);
  read_solution(argv[2]);
  return 0;
}

