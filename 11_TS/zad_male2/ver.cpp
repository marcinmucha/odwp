#include <sstream>
#include <string>
#include <set>
#include <thread>
#include <cassert>
#include <iostream>
#include <vector>

using namespace std;

const int N = 200000;
int weight[N];
vector<int> elements[N];
int n,m;
char txt[N];


void read_input(char *s) {
  FILE *f = fopen(s, "r");
  fgets(txt, N-1, f);
  sscanf(txt, "%d %d", &n, &m);
  fprintf(stderr, "n=%d m=%d\n", n, m);
  for (int i = 0; i < m; ++i) {
    fgets(txt, N-1, f);
    istringstream in(txt);
    in >> weight[i];
    int x;
    while (in >> x) elements[i].push_back(x);
  }
  fclose(f);
}


void read_solution(char *s) {
  FILE *f = fopen(s, "r");
  set<int> sets;
  int x;
  while (fscanf(f, "%d", &x) == 1) {
    assert(x >= 0 && x < m);
    sets.insert(x);
  }

  int total = 0;
  for (auto x : sets) total += weight[x];
  fprintf(stderr, "%lu sets of total weight %d\n", sets.size(), total);

  set<int> covered;
  for (auto x : sets) for (auto y : elements[x]) covered.insert(y);
  assert(covered.size() == n);
  fprintf(stderr, "OK\n");
  fclose(f);
}

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "Usage %s infile outfile\n", argv[0]);
    return 1;
  }
  read_input(argv[1]);
  read_solution(argv[2]);
  return 0;
}

