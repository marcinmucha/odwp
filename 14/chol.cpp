#include <iostream>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

int main(){
    Matrix<float,3,3> A;
    A << 1, 0, 0.8, 0, 1, 0, 0.8, 0, 1;
    cout << "A:" << endl << A << endl;
    auto chol = A.llt();
    Matrix<float,3,3> L;
    chol.matrixL().evalTo(L);
    cout << "L:" << endl;
    cout << L << endl;
    cout << "LL^T:" << endl;
    cout << L*L.transpose() << endl;
    return 0;
}
