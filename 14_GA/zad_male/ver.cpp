#include <algorithm>
#include <iostream>
#include <string>
#include <cassert>
#include <vector>
using namespace std;

#define MAX_LEN 200
#define M 10

inline int squares(const string &s)
{
  int n = s.size();
  int kmr[M][MAX_LEN]; //slownik slow bazowych

  assert(n <= MAX_LEN);
  assert((1 << M) > n);
  for (int i = 0; i < n; ++i) kmr[0][i]=s[i]-'0';
  int pot=1;
  for (int ii = 1; ii < M; ++ii) {
    vector<pair<pair<int,int>,int>> pom;
    for (int i = 0; i+2*pot <= n; ++i) pom.push_back(make_pair(make_pair(kmr[ii-1][i],kmr[ii-1][i+pot]),i));
    sort(pom.begin(), pom.end());
    int i=0,nr=0;
    while (i < pom.size()) {
      int i0=i;
      while (i0<pom.size() && pom[i0].first==pom[i].first) {
        kmr[ii][pom[i0].second]=nr;
        i0++;
      }
      i=i0; nr++;
    }
    pot*=2;
  }

  int res = 0;
  for (int len = 1; len <= n/2; ++len) {
    vector<pair<int,int>> codes;
    int pot=1,ii=0;
    while (2*pot<len) { ii++; pot*=2; }
    for (int i = 0; i+2*len <= n; ++i) {
      if (kmr[ii][i] == kmr[ii][i+len] && kmr[ii][i+len-pot] == kmr[ii][i+2*len-pot])
        codes.push_back(make_pair(kmr[ii][i],kmr[ii][i+len-pot]));
    }
    sort(codes.begin(), codes.end());
    codes.erase(unique(codes.begin(), codes.end()), codes.end());
    res += codes.size();
  }
  return res;
}

int main() {
  string s;
  cin >> s;
  for (auto c : s) assert(c >= '0' && c <= '1');
  cerr << "n = " << s.size() << endl;
  cerr << "squares = " << squares(s) << endl;
  return 0;
}
