W pierwszym wierszu liczba pracownikow n. Pracownicy sa numerowanie 0..n-1.
W kazdym nastepnym opis kolejnego pracownika w formacie:
ojciec_1 ojciec_2 minimum_1 minimum_2
gdzie ojciec_k to szef pracownika w hierarchii k - jesli jest on korzeniem, to uznajemy, ze
sam jest swoim ojcem. minimum_k to minimalna liczba pracownikow w odpowiednim poddrzewie.
