#include <vector>
#include <iostream>

#include "gurobi_c++.h"

#define FOR(i,a,b)  for(__typeof(b) i=(a);i<(b);++i)
#define REP(i,a)    FOR(i,0,a)
#define FOREACH(x,c)   for(__typeof(c.begin()) x=c.begin();x != c.end(); x++)
#define ALL(c)      c.begin(),c.end()
#define CLEAR(c)    memset(c,0,sizeof(c))
#define SIZE(c) (int) ((c).size())

#define VC	    vector
#define VI          VC<int>
#define VD          VC<double>
#define VS          VC<string>
#define VII         VC<II>
#define VDD         VC<DD>

#define DB(a)       cerr << #a << ": " << a << endl;

using namespace std;

void solveLP(int n){
    GRBEnv *env = new GRBEnv();
    env->set(GRB_DoubleParam_TimeLimit,600);

    GRBModel *model = new GRBModel(*env);
    model->set(GRB_IntAttr_ModelSense,1); // niepotrzebne, ale tak mozna to zmienic

    GRBVar a = model->addVar(-GRB_INFINITY,GRB_INFINITY,0,GRB_CONTINUOUS,string("a"));
    GRBVar b = model->addVar(-GRB_INFINITY,GRB_INFINITY,0,GRB_CONTINUOUS,string("b"));
    VC<GRBVar> r(n);
    REP(i,n) r[i] = model->addVar(0,GRB_INFINITY,1,GRB_CONTINUOUS,string("r_")+to_string(i));

    model->update(); // bez tego constrainty nie pojda, zmiana atrybutow zmiennych tez nie

    REP(i,n){
        int x=rand()%n, y=rand()%n;
        model->addConstr(r[i] >= (y-x*a-b)); // tak wolno!
        model->addConstr(r[i] >= -(y-x*a-b));
    }

    model->update(); // bez tego zoptymalizuje ok ale wypisze zle

    model->write("model.lp");

    model->optimize();

    cout << "a=" << a.get(GRB_DoubleAttr_X) << " b=" << b.get(GRB_DoubleAttr_X) << endl;

    delete model;
    delete env;
}

int main(int argc, char *argv[]){
    srand(time(NULL));
    int n;
    if (argc > 1)
        n = atoi(argv[1]);
    else
        n = 3;
    solveLP(n);
    return 0;
}
