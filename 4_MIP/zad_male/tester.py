import sys

if len(sys.argv) < 3:
	print "python tester.py input solution"
	exit(1)

solLines = open(sys.argv[2]).readlines()

claimedScore = int(solLines[0])
claimedSize = int(solLines[1])

sol = {}
solCnt = 26*[0]

for i in range(claimedSize):
	s = solLines[2+i].strip()
	if s not in sol:
		sol[s] = 0
	sol[s] = sol[s] + 1
	for c in s:
		solCnt[ord(c)-ord('a')] = solCnt[ord(c)-ord('a')]+1
print "Solution: " +str(solCnt)

dataLines = open(sys.argv[1]).readlines()
bounds = map(int,dataLines[0].split())
print "Bounds:   "+str(bounds)
for i in range(26):
	if solCnt[i] > bounds[i]:
		print "Too many %c's"%(chr(i+ord('a')))
		exit(1)
print "Counts OK"

n = int(dataLines[1])
score = 0
for i in range(n):
	l = dataLines[2+i].split()
	wb = int(l[0])
	sb = int(l[1])
	
	d = {}
	for w in l[2:]:
		ws = w.strip()
		if ws not in d:
			d[ws] = 0
		d[ws] = d[ws] + 1
	bonus = 1
	hits = 0
	for w,cnt in d.items():
		if w not in sol or cnt > sol[w]:
			bonus = 0
		if w in sol:
			hits = hits + min(sol[w],cnt)
	score = score + hits*wb + bonus*sb
print "Claimed score: %d"%claimedScore
print "Actual score:  %d"%score
if claimedScore != score:
	print "Ba ba ba ba"
	exit(1)
print "CORRECT"
