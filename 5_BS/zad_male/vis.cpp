#include "CImg.h"
#include <thread>
#include <cassert>
#include <iostream>

using namespace std;
using namespace cimg_library;

const int R = 6;
const int MAX_K = 100;
int K = 10;
int turn_length = 10000;

int cell[MAX_K][MAX_K];
unsigned char blue[] = {0,0,255};
unsigned char red[] = {255,0,0};

int x = 10;
int y = 0;
int vx = 1; 
int vy = 1;

CImg<unsigned char> draw() {
  CImg<unsigned char> img(4 * K * R, (4 * K + 20) * R, 1, 3); 
  img.fill(0);                           // Set pixel values to 0 (color : black)
  for (int i = 0; i < K; ++i)
    for (int j = 0; j < K; ++j) 
      if (cell[i][j]) 
        img.draw_rectangle(4 * i * R + 1, (4 * j + 20) * R + 1, 4 * (i+1) * R - 1, (4 * (j+1) + 20) * R - 1, blue);
  img.draw_circle(x * R + R/2, y * R + R/2, R/2, red);
  return img;
}

void visualize() {
  CImgDisplay disp;
  while (true) {
    disp = draw();
  }
}

inline int crosses(int x, int y) {
  if (y < 20) return 0;
  return cell[x/4][(y-20)/4];
}

inline void kill(int x,int y){
  assert(y >= 20);
  int &r = cell[x/4][(y-20)/4];
  assert(r == 1);
  r = 0;
}


int simulate() {
  
  int res = 1;
  while (true) {
    usleep(turn_length);
    if (res % 2) {
      int nx = x + vx;
      if (nx < 0 || nx >= 4 * K) vx = -vx;
      else {
        if (crosses(nx,y)) {
          kill(nx,y);
          vx = -vx;
        } else {
          x = nx;
        }
      }
    } else {
      int ny = y + vy;
      if (ny >= 4 * K + 20) {
        vy = -vy;
      } else if (ny < 0) break;
      else if (crosses(x, ny)){
        kill(x, ny);
        vy = -vy;
      } else {
        y = ny;
      }
    }
    res++;
  }
  fprintf(stderr, "Score = %d\n", res);
  return res;
}

void read_cells() {
  for (int i = 0; i < K; ++i) {
    string s;
    cin >> s;
    assert(s.size() == K);
    for (int j = 0; j < K; ++j) {
      assert(s[j] == '0' || s[j] == '1');
      cell[i][j] = s[j]-'0';
    }
  }
}

int main(int argc, char **argv) {
  if (argc < 2 || argc > 3) {
    fprintf(stderr, "Usage %s n [turn_length]\n", argv[0]);
    return 1;
  }
  K = atoi(argv[1]);
  if (argc > 2) turn_length = atoi(argv[2]);
  read_cells();
  thread t(visualize);
  simulate();
  t.detach();
  return 0;
}

