#include <cassert>
#include <cstdio>

const int N = 100000;

int row[N];
int col[N];
int diag1[2*N];
int diag2[2*N];

int main(){
  int n;
  scanf("%d",&n);
  assert(n >= 1 && n < N);
  for (int i = 0; i < n; ++i) {
    int a,b;
    int c = scanf("%d %d",&a,&b);
    assert(c == 2);
    assert(a >= 0 && a < n && b >= 0 && b < n);
    assert(row[a] == 0 && col[b] == 0 && diag1[a+b] == 0 && diag2[n-a+b] == 0);
    row[a]++; col[b]++; diag1[a+b]++; diag2[n-a+b]++;
  }
  printf("OK n=%d\n", n);
  return 0;
}
