#include <cassert>
#include <cstdio>

const int N = 1000;

int used[N*N];
int row[N];
int col[N];
int diag1;
int diag2;

int main(){
  int n;
  scanf("%d",&n);
  assert(n >= 1 && n < N-1);
  for (int i = 0; i < n; ++i) 
    for (int j = 0; j < n; ++j) {
      int x;
      scanf("%d",&x);
      assert(x >= 1 && x <= n * n);
      assert(!used[x]);
      used[x] = 1;
      row[i] += x;
      col[j] += x;
      if (i == j) diag1 += x;
      if (n-i-1 == j) diag2 += x;
    }
  int magic = n * (n*n + 1) / 2;
  assert(diag1 == magic);
  assert(diag2 == magic);
  for (int i = 0; i < n; ++i) {
    assert(row[i] == magic && col[i] == magic);
  }
  printf("OK n=%d\n", n);
  return 0;
}
