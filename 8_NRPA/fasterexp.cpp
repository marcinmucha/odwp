static inline float fasterpow2 (float p){
    float clipp = (p < -126) ? -126.0f : p;
    union { unsigned int i; float f; } v = { static_cast<unsigned int> ( (1 << 23) * (clipp + 126.94269504f) ) };
    return v.f;
}

static inline float fasterexp (float p){
    return fasterpow2 (1.442695040f * p);
}
