#include "CImg.h"
#include <set>
#include <thread>
#include <cassert>
#include <iostream>

using namespace std;
using namespace cimg_library;

unsigned char blue[] = {0,0,255};
unsigned char red[] = {255,0,0};
unsigned char black[] = {0, 0, 0};

const int N = 110 * 1000;
const int H = 500;
int n;
double x[N];
double y[N];
int xx[N], yy[N];
int perm[N];

CImg<unsigned char> draw() {
  CImg<unsigned char> img(H,H,1,3); 
  img.fill(0xff);                           // Set pixel values to 0 (color : black)
  for (int i = 0; i < n; ++i) {
    xx[i] = x[i] * H;
    yy[i] = y[i] * H;
  }
  double sum = 0;
  for (int i = 0; i < n; ++i) {
    int a = perm[i];
    int b = perm[(i+1) % n];
    sum += hypot(x[a]-x[b], y[a]-y[b]);
    img.draw_line(xx[a], yy[a], xx[b], yy[b], black);
  }
  fprintf(stderr, "cost=%.6lf\n", sum);
  for (int i = 0; i < n; ++i)
    img.draw_circle(xx[i], yy[i], 2, blue);
  return img;
}

void visualize() {
  CImgDisplay disp;
  disp = draw();
  while (!disp.is_closed()) usleep(100 * 1000);
}


void read_vertices(char *s) {
  FILE *f = fopen(s, "r");
  fscanf(f, "%d", &n);
  fprintf(stderr, "n=%d\n", n);
  assert(n < N);
  for (int i = 0; i < n; ++i) fscanf(f, "%lf %lf", x+i, y+i);
  fclose(f);
}

void read_solution(char *s) {
  FILE *f = fopen(s, "r");
  set<int> set_ver;
  for (int i = 0; i < n; ++i) {
    fscanf(f, "%d", perm+i);
    assert(perm[i] >= 0 && perm[i] < n);
    assert(set_ver.count(perm[i]) == 0);
    set_ver.insert(perm[i]);
  }
  fclose(f);
}

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "Usage %s infile outfile\n", argv[0]);
    return 1;
  }
  read_vertices(argv[1]);
  read_solution(argv[2]);
  visualize();
  return 0;
}

