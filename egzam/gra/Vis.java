import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.lang.Math.*;
import java.security.*;
import java.util.*;
import javax.swing.*;

class ColorCaptureAI {
    SecureRandom r1;
    int player;
    public ColorCaptureAI(int s, int p) {
        player = p;
    try {
        r1 = SecureRandom.getInstance("SHA1PRNG");
        r1.setSeed(s);
    }
    catch (NoSuchAlgorithmException e) { }
    }
    int getColor(char c) {
        return (int)(c - 'A');
    }
    int makeTurn(String[] board) throws NoSuchAlgorithmException {
        int D = board.length;
        // get list of all colors present on the board adjacent to area controlled by player
        // bfs starting with corner cell already controlled by player
        int[] r = new int[D * D];
        int[] c = new int[D * D];
        boolean[][] vis = new boolean[D][D];
        int n = 0;

        // starting point depends on the player
        int r0, c0;
        r0 = c0 = (player == 0 ? 0 : D - 1);
        char playerColor = board[r0].charAt(c0);

        vis[r0][c0] = true;
        r[n] = r0;
        c[n] = c0;
        ++n;

        final int[] dr = {0, 1, 0, -1};
        final int[] dc = {-1, 0, 1, 0};
        int col0 = getColor(board[0].charAt(0));
        int ret = 2;
        int ret_coord = D * D * D;

        for (int ind = 0; ind < n; ++ind)
        for (int d = 0; d < 4; ++d) {
            int newr = r[ind] + dr[d];
            int newc = c[ind] + dc[d];
            if (newr < 0 || newc < 0 || newr >= D || newc >= D)
                continue;
            if (!vis[newr][newc]) {
                if (board[newr].charAt(newc) == playerColor) {
                    vis[newr][newc] = true;
                    r[n] = newr;
                    c[n] = newc;
                    ++n;
                } else {
                    // adjacent to one of player's cells but of different color - keep
                    int col = getColor(board[newr].charAt(newc));
                    int cand = Math.min(newr, newc) * D * D + newr * D + newc;
                    if (col != col0 && cand < ret_coord) {
                      ret = col;
                      ret_coord = cand;
                    }
                }
            }
        }

        return ret;
    }
}

public class Vis {
    static int maxSize = 100, minSize = 100;
    static int maxColors = 16, minColors = 4;

    final int[] dr = {0, 1, 0, -1};
    final int[] dc = {-1, 0, 1, 0};

    int D;              // dimension of the board in pixels
    int C;              // number of colors in the game
    int[][] map;        // which color each region is
    int[][] control;    // which player controls each region (if any)
    int[] area;         // area controlled by players 0 and 1, respectively

    SecureRandom r1;

    int turnN;
    // -----------------------------------------
    char getChar(int color) {
        return (char)('A' + color);
    }
    // -----------------------------------------
    String[] getBoard() {
        String[] board = new String[D];
        for (int i = 0; i < D; ++i) {
            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < D; ++j)
                sb.append(getChar(map[i][j]));
            board[i] = sb.toString();
        }
        return board;
    }
    // -----------------------------------------
    void updateArea(int player) {
        // step 1. update area controlled by the player by virtue of being connected to his color
        // (or previously being under his control)
        int playercolor = player == 0 ? map[0][0] : map[D - 1][D - 1];
        area[player] = 0;
        // bfs starting with cells already controlled by that player
        int[] r = new int[D * D];
        int[] c = new int[D * D];
        boolean[][] vis = new boolean[D][D];
        int n = 0;
        for (int i = 0; i < D; ++i)
        for (int j = 0; j < D; ++j)
            if (control[i][j] == player) {
                vis[i][j] = true;
                r[n] = i;
                c[n] = j;
                ++n;
                ++area[player];
            }
        for (int ind = 0; ind < n; ++ind)
            for (int d = 0; d < 4; ++d) {
                int newr = r[ind] + dr[d];
                int newc = c[ind] + dc[d];
                if (newr < 0 || newc < 0 || newr >= D || newc >= D)
                    continue;
                if (!vis[newr][newc] && map[newr][newc] == playercolor) {
                    control[newr][newc] = player;
                    vis[newr][newc] = true;
                    r[n] = newr;
                    c[n] = newc;
                    ++n;
                    ++area[player];
                }
            }
    }
    // -----------------------------------------
    double getScore(int player) {
        return area[player];
        // at any moment score is percentage of the board controlled by player
    }
    // -----------------------------------------
    boolean endGame() {
        // * all area is controlled by one of the players
        // * D^2 turns have been done (safety net)
        return area[0] + area[1] == D * D || turnN == D * D + 1;
    }
    // -----------------------------------------
    void generate(String seedStr) {
    try {
        // generate test case
        r1 = SecureRandom.getInstance("SHA1PRNG");
        long seed = Long.parseLong(seedStr);
        r1.setSeed(seed);
        D = r1.nextInt(maxSize - minSize + 1) + minSize;
        C = r1.nextInt(maxColors - minColors + 1) + minColors;
        if (seed <= 3) {
            C = minColors + 4 * (int)(seed - 1);
        }
        if (seed == 4) {
            C = maxColors;
        }
        if (seed == 1) C = minColors+1;

        // generate the map of cell colors
        map = new int[D][D];
        for (int i = 0; i < D; ++i)
        for (int j = 0; j < D; ++j) {
            map[i][j] = r1.nextInt(C-2) + 2;
        }
        map[0][0] = 0;
        map[D-1][D-1] = 1;

        // set starting region controls
        control = new int[D][D];
        for (int i = 0; i < D; ++i) {
            Arrays.fill(control[i], -1);
        }
        control[0][0] = 0;
        control[D - 1][D - 1] = 1;

        // update areas controlled by players (can be greater than 1 from the start)
        area = new int[2];
        updateArea(0);
        updateArea(1);

        if (debug) {
            System.out.println("D = " + D);
            System.out.println("C = " + C);
            System.out.println("Starting board: ");
            String[] b = getBoard();
            for (String st : b) {
                System.out.println(st);
            }
            System.out.println("Player 0 area = " + area[0]);
            System.out.println("Player 1 area = " + area[1]);
        }

        if (vis) {
            W = D * SZ + 40 + SZ * 4;
            H = D * SZ + 40;
        }
    }
    catch (Exception e) {
        System.err.println("An exception occurred while generating test case.");
        e.printStackTrace();
    }
    }
    // -----------------------------------------
    public double runTest(String seed) {
    try {
        generate(seed);

        if (vis) {
            jf.setSize(W, H);
            jf.setVisible(true);
            draw();
        }

        ColorCaptureAI ai = new ColorCaptureAI(r1.nextInt(), 1);
        turnN = 1;
        int timeUsed = 0;          // keep track only of human player time
        while (!endGame()) {
            String[] board = getBoard();
            // decide which method to call - human or AI - based on turn #
            int player = (turnN - 1) % 2;
            long startTime = System.currentTimeMillis();
            int color = player == 0 ? makeTurn(board) : ai.makeTurn(board);
            if (player == 0) {
                timeUsed += (int)(System.currentTimeMillis() - startTime);
            }

            // check the returned color for validity
            if (color < 2 || color >= C) {
                addFatalError("Turn #" + turnN + ": return is not a valid color: " + color + ".");
                return 0.0;
            }
            // check that it's not equal to previous color or opponent's current color
            if (color == map[0][0] || color == map[D - 1][D - 1]) {
                addFatalError("Turn #" + turnN + " player #" + player + " color #" + color + ": return is the same as your previous color or opponent's color.");
                return 0.0;
            }

            // perform color switch and update controlled cells accordingly
            for (int i = 0; i < D; ++i)
            for (int j = 0; j < D; ++j)
                if (control[i][j] == player) {
                    map[i][j] = color;
                }
            updateArea(player);
            for (int i = 0; i < D; ++i)
            for (int j = 0; j < D; ++j)
                if (control[i][j] == player) {
                    map[i][j] = player;
                }

            if (vis) {
                draw();
            }
            if (debug) {
                System.out.println("Turn #" + turnN + ": player 0 = " + area[0] + ", player 1 = " + area[1]);
            }
            ++turnN;
        }

        System.out.println((turnN - 1) + " turns done.");
        return getScore(0);
    }
    catch (Exception e) {
        System.err.println("An exception occurred while trying to get your program's results.");
        e.printStackTrace();
        return 0;
    }
    }
// ------------- visualization part ------------
    JFrame jf;
    Visual v;
    static String file_name;
    static boolean vis, debug;
    static Process proc;
    static int del;
    InputStream is;
    OutputStream os;
    BufferedReader br;
    String commands;
    int commands_pos;
    static int SZ, W, H;
    // -----------------------------------------
    int makeTurn(String[] board) throws IOException {
        commands_pos += 1;
        return commands.charAt(commands_pos-1) - 'A';
    }
    // -----------------------------------------
    void draw() {
        if (!vis) return;
        v.repaint();
        try { Thread.sleep(del); }
        catch (Exception e) { };
    }
    // -----------------------------------------
    BufferedImage cache;
    Color[] colors;
    int[] cs = {
        0x000000,
        0xFFFFFF,
        0xFFE300,
        0x0000FF,
        0xFF6800,
        0xA6BDD7,
        0xFF0000,
        0xCEA262,
        0x817066,
        0x007D34,
        0xF6869E,
        0x00538A,
        0x7F180D,
        0x93AA00,
        0xFF00FF,
        0x702E65};
    HashMap<Integer, Integer> colorMap;
    void GeneratePalette() {
        colors = new Color[C];
        colorMap = new HashMap<>();
        for (int i = 0; i < C; ++i) {
            colors[i] = new Color(cs[i]);
            colorMap.put(cs[i], i);
        }
    }
    // -----------------------------------------
    public class Visual extends JPanel implements MouseListener, WindowListener {
        public void paint(Graphics g) {
            if (colors == null) {
                GeneratePalette();
            }
            cache = new BufferedImage(W, H, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = (Graphics2D)cache.getGraphics();
            // background
            g2.setColor(new Color(0xDDDDDD));
            g2.fillRect(0, 0, W, H);

            // current colors of the cells of the board (draw every cell)
            for (int i = 0; i < D; ++i)
                for (int j = 0; j < D; ++j) {
                    g2.setColor(colors[map[i][j]]);
                    g2.fillRect(j * SZ, i * SZ, SZ, SZ);
                }

            g.drawImage(cache,0,0,W,H,null);
        }
        // -------------------------------------
        public Visual() {
            addMouseListener(this);
            jf.addWindowListener(this);
        }
        // -------------------------------------
        // WindowListener
        public void windowClosing(WindowEvent e){
            if (proc != null)
                try { proc.destroy(); }
                catch (Exception ex) { ex.printStackTrace(); }
            System.exit(0);
        }
        public void windowActivated(WindowEvent e) { }
        public void windowDeactivated(WindowEvent e) { }
        public void windowOpened(WindowEvent e) { }
        public void windowClosed(WindowEvent e) { }
        public void windowIconified(WindowEvent e) { }
        public void windowDeiconified(WindowEvent e) { }
        // -------------------------------------
        // MouseListener
        public void mouseClicked(MouseEvent e) {
          return;
        }
        public void mousePressed(MouseEvent e) { }
        public void mouseReleased(MouseEvent e) { }
        public void mouseEntered(MouseEvent e) { }
        public void mouseExited(MouseEvent e) { }
    }
    // -----------------------------------------
    public Vis(String seed) {
      try {
        if (vis)
        {   jf = new JFrame();
            v = new Visual();
            jf.getContentPane().add(v);
        }
        InputStream fis = new FileInputStream(file_name);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        commands = br.readLine();
        commands_pos = 0;
        System.out.println("commands = " + commands);
        System.out.println("Score = " + runTest(seed));
        if (proc != null)
            try { proc.destroy(); }
            catch (Exception e) { e.printStackTrace(); }
      }
      catch (Exception e) { e.printStackTrace(); }
    }
    // -----------------------------------------
    public static void main(String[] args) {
        String seed = "1";
        vis = true;
        del = 100;
        SZ = 10;
        for (int i = 0; i<args.length; i++)
        {   if (args[i].equals("-seed"))
                seed = args[++i];
            if (args[i].equals("-file"))
                file_name = args[++i];
            if (args[i].equals("-delay"))
                del = Integer.parseInt(args[++i]);
            if (args[i].equals("-novis"))
                vis = false;
            if (args[i].equals("-size"))
                SZ = Integer.parseInt(args[++i]);
            if (args[i].equals("-debug"))
                debug = true;
        }
        Vis f = new Vis(seed);
    }
    // -----------------------------------------
    void addFatalError(String message) {
        System.out.println(message);
    }
}

class ErrorReader extends Thread{
    InputStream error;
    public ErrorReader(InputStream is) {
        error = is;
    }
    public void run() {
        try {
            byte[] ch = new byte[50000];
            int read;
            while ((read = error.read(ch)) > 0)
            {   String s = new String(ch,0,read);
                System.out.print(s);
                System.out.flush();
            }
        } catch(Exception e) { }
    }
}
