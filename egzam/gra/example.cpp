#include <cassert>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
#include <set>

using namespace std;

const int N = 100;
int n = 100;
char board[N][N+1];

int area[2];

void read_board() {
  for (int i = 0; i < n; ++i) scanf("%s", board[i]);
  assert(board[0][0] == 'A' && board[n-1][n-1] == 'B');
}

int vis[N][N];
int dx[] = {0,0,1,-1};
int dy[] = {1,-1,0,0};

inline int between(int a, int b, int c) {
  return a >= b && a <= c;
}

int make_move() {
  vector<pair<int,int>> v;
  v.push_back(make_pair(0,0));
  for (int i = 0; i < n; ++i) 
    for (int j = 0; j < n; ++j)
      vis[i][j] = 0;
  vis[0][0] = 1;

  set<char> colors;
  for (int i = 0; i < (int)v.size(); ++i) {
    int x = v[i].first;
    int y = v[i].second;
    for (int r = 0; r < 4; ++r) {
      int nx = x + dx[r];
      int ny = y + dy[r];
      if (!between(nx, 0, n-1) || !between(ny, 0, n-1)) continue;
      if (board[nx][ny] != 'B' && board[nx][ny] != 'A') {
        colors.insert(board[nx][ny]);
      } else if (board[nx][ny] == 'A' && !vis[nx][ny]) {
        vis[nx][ny] = 1;
        v.push_back(make_pair(nx, ny));
      }
    }
  }
  vector<char> colors_v(colors.begin(), colors.end());
  return colors_v.empty() ? 'C' : colors_v[rand() % (int)colors_v.size()];
}

int simulate_opponent() {
  vector<pair<int,int>> v;
  v.push_back(make_pair(n-1,n-1));
  for (int i = 0; i < n; ++i) 
    for (int j = 0; j < n; ++j)
      vis[i][j] = 0;
  vis[n-1][n-1] = 1;

  char res = 'C';
  int res_coord = n * n * n;
  for (int i = 0; i < (int)v.size(); ++i) {
    int x = v[i].first;
    int y = v[i].second;
    for (int r = 0; r < 4; ++r) {
      int nx = x + dx[r];
      int ny = y + dy[r];
      if (!between(nx, 0, n-1) || !between(ny, 0, n-1)) continue;
      if (board[nx][ny] != 'B' && board[nx][ny] != 'A') {
        int cand = min(nx, ny) * n * n + nx * n + ny;
        if (cand < res_coord) {
          res_coord = cand;
          res = board[nx][ny];
        }
      } else if (board[nx][ny] == 'B' && !vis[nx][ny]) {
        vis[nx][ny] = 1;
        v.push_back(make_pair(nx, ny));
      }
    }
  }
  return res;
}

void update_area(int player, int color) {
  vector<pair<int,int>> v;
  int x, y;
  x = y = player == 0 ? 0 : n-1;
  v.push_back(make_pair(x, y));
  for (int i = 0; i < n; ++i) 
    for (int j = 0; j < n; ++j)
      vis[i][j] = 0;
  vis[x][y] = 1;

  for (int i = 0; i < (int)v.size(); ++i) {
    int x = v[i].first;
    int y = v[i].second;
    for (int r = 0; r < 4; ++r) {
      int nx = x + dx[r];
      int ny = y + dy[r];
      if (!between(nx, 0, n-1) || !between(ny, 0, n-1)) continue;
      if (board[nx][ny] == color) {
        board[nx][ny] = 'A' + player;
        vis[nx][ny] = 1;
        v.push_back(make_pair(nx, ny));
        area[player]++;
      } else if (board[nx][ny] == 'A' + player && !vis[nx][ny]) {
        vis[nx][ny] = 1;
        v.push_back(make_pair(nx, ny));
      }
    }
  }
}

void play() {
  area[0] = area[1] = 1;
  int turn = 0;
  string res;
  while (area[0] + area[1] < n * n) {
    char color;
    if (turn % 2 == 0) {
      color = make_move();
      res += color;
    } else color = simulate_opponent();
    update_area(turn % 2, color);
    turn++;
    //fprintf(stderr, "turn %d, area[0] = %d, area[1] = %d, color = %c\n", turn, area[0], area[1], color);
  }
  fprintf(stderr, "area[0] = %d area[1] = %d\n", area[0], area[1]);
  printf("%s\n", res.c_str());
}

int main() {
  read_board();
  play();
  return 0;
}
