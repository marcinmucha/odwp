#include <iostream>
#include <fstream>
#include <set>
#include <vector>

using namespace std;

int n,m;
vector<pair<int,int>> E;

int claimed;
set<int> S;

void readTest(const char *fname){
    ifstream f(fname);
    f >> n >> m;
    E.resize(m);
    for(int i=0; i < m; i++)
        f >> E[i].first >> E[i].second;
    f.close();
}

void readOut(const char *fname){
    ifstream f(fname);
    f >> claimed;
    int x;
    for(int i=0; i < n/2; i++){
        f >> x;
        S.insert(x);
    }
    f.close();
    if (S.size() != n/2){
        cerr << "|S|=" << S.size() << " should be " << n/2 << endl;
        exit(1);
    }
}

void score(){
    int score = 0;
    for(auto &e : E){
        int in1 = S.find(e.first) != S.end();
        int in2 = S.find(e.second) != S.end();
        score += in1 != in2;
    }
    if (score != claimed){
        cerr << "Score: " << score << " claimed: " << claimed << endl;
        exit(1);
    }
    cerr << "Score: " << score << " all OK!" << endl;
}

int main(int argc, char *argv[]){
    if (argc < 3){
        cerr << "Use: ./verify test out" << endl;
        exit(1);
    }
    readTest(argv[1]);
    readOut(argv[2]);
    score();
}
