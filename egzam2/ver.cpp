#include "CImg.h"
#include <set>
#include <thread>
#include <cassert>
#include <iostream>
#include <vector>

using namespace std;
using namespace cimg_library;

unsigned char blue[] = {0,0,255};
unsigned char red[] = {255,0,0};
unsigned char black[] = {0, 0, 0};

const int N = 110 * 1000;
const int H = 500;
int n, k;
double penalty;
double x[N];
double y[N];
int xx[N], yy[N];
int perm[N];
vector<int> path[N];

double calc_dist(int a, int b) {
  return hypot(x[a]-x[b], y[a]-y[b]);
}

CImg<unsigned char> draw() {
  CImg<unsigned char> img(H,H,1,3); 
  img.fill(0xff);                           // Set pixel values to 0 (color : black)
  for (int i = 0; i < n+k; ++i) {
    xx[i] = x[i] * H;
    yy[i] = y[i] * H;
  }
  double score = 0;

  for (int i = 0; i < k; ++i) {
    int last = n+i;
    double path_cost = 0.0;
    for (auto c : path[i]) {
      img.draw_line(xx[last], yy[last], xx[c], yy[c], black);
      path_cost += calc_dist(last, c);
      last = c;
    }
    img.draw_line(xx[last], yy[last], xx[n+i], yy[n+i], black);
    path_cost += calc_dist(last, n+i);
    score = max(score, path_cost);
 //   fprintf(stderr, "path cost=%.6lf\n", path_cost);
  }
  fprintf(stderr, "cost=%.6lf\n", score);

  for (int i = 0; i < n+k; ++i)
    img.draw_circle(xx[i], yy[i], 2, i < n ? blue : red);
  return img;
}

void visualize() {
  CImgDisplay disp;
  disp = draw();
  while (!disp.is_closed()) usleep(100 * 1000);
}

void read_vertices(char *s) {
  FILE *f = fopen(s, "r");
  fscanf(f, "%d %d", &n, &k);
  fprintf(stderr, "n=%d k=%d b=%.6lf\n", n, k, penalty);
  assert(n+k < N);
  for (int i = 0; i < n+k; ++i) fscanf(f, "%lf %lf", x+i, y+i);
  fclose(f);
}

void read_solution(char *s) {
  FILE *f = fopen(s, "r");
  set<int> set_ver;
  double res = 0.0;
  for (int i = 0; i < k; ++i) {
    int d;
    fscanf(f, "%d", &d);
    assert(d >= 0 && d <= n);
    for (int j = 0; j < d; ++j) {
      int x;
      fscanf(f, "%d", &x);
      //cerr << "x: " << x << endl;
      assert(x >= 1 && x <= n);
      assert(set_ver.count(x) == 0);
      set_ver.insert(x);
      path[i].push_back(x-1);
    }
  }
  assert(set_ver.size() == n);
  fclose(f);
}

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "Usage %s infile outfile\n", argv[0]);
    return 1;
  }
  read_vertices(argv[1]);
  read_solution(argv[2]);
  visualize();
  return 0;
}

